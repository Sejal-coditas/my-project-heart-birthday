const bodyEl = document.querySelector("body")
bodyEl.addEventListener("mousemove", (event)=>{
    // get the cursor coordinate
    const xPos = event.offsetX
    const yPos = event.offsetY
    // create new span
    const spanEl = document.createElement("span");
    // position teh span according rto curosr
    spanEl.style.left = xPos+"px";
    spanEl.style.top = yPos+"px";
    // create random size
    const size = Math.random()*100;
    spanEl.style.width = size+"px"
    spanEl.style.height = size+"px"
    // add new span to body
    bodyEl.appendChild(spanEl);
    // remove after 3sec
    setTimeout(()=>{
        spanEl.remove();
    },3000)
})